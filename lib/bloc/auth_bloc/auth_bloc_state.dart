part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState {}

class AuthBlocInitialState extends AuthBlocState { }

class AuthBlocLoadingState extends AuthBlocState { }

class AuthBlocLoggedInState extends AuthBlocState { }

class AuthBlocFailedLoginState extends AuthBlocState { }

class AuthBlocSuccesLoginState extends AuthBlocState { }

class AuthBlocLoginState extends AuthBlocState { }

class AuthBlocSuccesState extends AuthBlocState { }

class AuthBlocLoadedState extends AuthBlocState {
    final data;

    AuthBlocLoadedState(this.data);

    @override
    List<Object> get props => [data];
}

class AuthBlocErrorState extends AuthBlocState {}
