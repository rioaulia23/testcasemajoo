import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/database/database_query.dart';
import 'package:majootestcase/database/db_helper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async{
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn== null){
      emit(AuthBlocLoginState());
    }else{
      if(isLoggedIn){
        emit(AuthBlocLoggedInState());
      }else{
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(User user) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocSuccesLoginState());
    await sharedPreferences.setBool("is_logged_in",true);
    String data = user.toJson().toString();
    sharedPreferences.setString("user_value", data);
    emit(AuthBlocLoggedInState());
  }

  void register_user(User user) async{
    final DbHelper _helper = new DbHelper();
    _helper.insert(UserQuery.TABLE_NAME, user.toJson());
  }
  void login_userCheck(User user) async{
    final DbHelper _helper = new DbHelper();
     await _helper.getLogin(user.email, user.password);
     if(_helper.succes == true){
      login_user(user);
    }else{
      emit(AuthBlocFailedLoginState());
    }
  }
  void logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

}
