import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices{

  Future<MovieModel> getMovieList() async {
    MovieModel movieModel;

    try {
      Future.delayed(Duration(milliseconds: 500));

      print("https://api.themoviedb.org/3/trending/all/day?api_key=739ea42b278843bce7edd7608525c5d8");

      Response response = await Dio().get(
        "https://api.themoviedb.org/3/trending/all/day?api_key=739ea42b278843bce7edd7608525c5d8",

      );
      print(response.data.toString());

      if (response.data != null) {
        movieModel = MovieModel.fromJson(response.data);
        return movieModel;
      }
      return movieModel;
    } on DioError catch (e, stacktrace) {
      print(e);
      print(stacktrace);
      return movieModel;
    }
  }
}