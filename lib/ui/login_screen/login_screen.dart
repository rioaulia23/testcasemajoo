import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_screen/home_screen.dart';
import 'package:majootestcase/ui/register_screen/register_screen.dart';




class LoginScreen extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginScreen> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  AuthBlocCubit authBlocCubit = AuthBlocCubit();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder(
        cubit: authBlocCubit,
        builder: (context,state){
          return BlocListener(
            cubit: authBlocCubit,
            listener: (context, state) {
              if(state is AuthBlocLoggedInState){
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (_) => BlocProvider(
                      create: (context) => HomeBlocCubit()..fetching_data(),
                      child: HomeBlocScreen(),
                    ),
                  ),
                );
              }else if(state is AuthBlocFailedLoginState){
                Fluttertoast.showToast(
                    msg: "Login Gagal, periksa kembali inputan anda",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.white,
                    textColor: Colors.black,
                    fontSize: 16.0);
              }else if(state is AuthBlocSuccesLoginState){

                Fluttertoast.showToast(
                    msg: "Login Berhasil",
                    toastLength: Toast.LENGTH_LONG,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.white,
                    textColor: Colors.black,
                    fontSize: 16.0);
              }
            },
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Selamat Datang',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        // color: colorBlue,
                      ),
                    ),
                    Text(
                      'Silahkan login_screen terlebih dahulu',
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      height: 9,
                    ),
                    _form(),
                    SizedBox(
                      height: 50,
                    ),
                    CustomButton(
                      text: 'Login',
                      onPressed: handleLogin,
                      height: 100,
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    _register(),
                  ],
                ),
              ),
            ),
          );
        },

      )
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Container(
            height: 110,
            child: CustomTextFormField(
              context: context,
              controller: _emailController,
              isEmail: true,
              hint: 'Example@123.com',
              label: 'Email',
              validator: (val) {
                final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (val != null)
                  return pattern.hasMatch(val) ? null : 'email is invalid';
              },
            ),
          ),
          Container(
            height: 110,
            child: CustomTextFormField(
              context: context,
              label: 'Password',
              hint: 'password',
              controller: _passwordController,
              isObscureText: _isObscurePassword,
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => RegisterScreen()
            ),
          );
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin() async {
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null
       ) {


      User user = User(
          email: _email,
          password: _password,
      );
      authBlocCubit.login_userCheck(user);
    }else if(formKey.currentState?.validate() == false &&
        _email != null &&
        _password != null){
      Fluttertoast.showToast(
          msg: "Masukkan e-mail yang valid",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    }else if(_email == null &&
        _password == null){
      Fluttertoast.showToast(
          msg: "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    }
  }
}
