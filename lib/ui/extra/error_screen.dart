import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/home_screen/home_screen.dart';


class ErrorScreen extends StatelessWidget {
  final String message;
  final Function() retry;
  final Color textColor;
  final double fontSize;
  final double gap;
  final Widget retryButton;

  const ErrorScreen(
      {Key key,
      this.gap = 10,
      this.retryButton,
      this.message="",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.signal_cellular_connected_no_internet_4_bar_sharp),
            Text('No Internet Connection, Please check your connection'),
            Column(
                    children: [
                      SizedBox(
                        height: 50,
                      ),
                      retryButton ??
                          IconButton(
                            onPressed: () {

                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (_) => BlocProvider(
                                    create: (context) => HomeBlocCubit()..fetching_data(),
                                    child: HomeBlocScreen(),
                                  ),
                                ),
                              );
                            },
                            icon: Icon(Icons.refresh_sharp),
                          ),
                    ],
                  )

          ],
        ),
      ),
    );
  }
}
