

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

class DetailScreen extends StatefulWidget {
  String title;
  String posterPath;
  String releaseDate;
  String voteAverage;
  String overview;
  String popularity;

  DetailScreen(this.data);

  Results data;
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.network("https://image.tmdb.org/t/p/w500/"+widget.data.posterPath,
                fit: BoxFit.cover,
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: widget.data.title != null ? Text("Movie Name : "+widget.data.title, textDirection: TextDirection.ltr)
                    : Text("Movie Name : "+widget.data.name, textDirection: TextDirection.ltr)

              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: widget.data.releaseDate != null ? Text("Release Date : "+widget.data.releaseDate, textDirection: TextDirection.ltr)
                    : Text("Release Date : "+widget.data.firstAirDate , textDirection: TextDirection.ltr),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text("Rating : "+widget.data.voteAverage.toString(), textDirection: TextDirection.ltr),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text("Overview : "+widget.data.overview, textDirection: TextDirection.ltr),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text("Popularity : "+widget.data.popularity.toString(), textDirection: TextDirection.ltr),
              ),
            ],
          )
        ),
      ),
    );
  }
}