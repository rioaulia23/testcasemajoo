

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<RegisterScreen> {
  final _mailController = TextController();
  final _passController = TextController();
  final _usernameController = TextController();
  AuthBlocCubit authBlocCubit = AuthBlocCubit();
  bool _isObscurePassword = true;
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Register',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan isi data diri anda terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _buildForm(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Daftar',
                  onPressed: registerProcess,
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          Container(
            height: 110,
            child: CustomTextFormField(
              context: context,
              controller: _usernameController,
              isEmail: true,
              hint: 'Username',
              label: 'Username',

            ),
          ),
          Container(
            height: 110,
            child: CustomTextFormField(
              context: context,
              controller: _mailController,
              isEmail: true,
              hint: 'Example@123.com',
              label: 'Email',
              validator: (val) {
                final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
                if (val != null)
                  return pattern.hasMatch(val) ? null : 'email is invalid';
              },
            ),
          ),
          Container(
            height: 110,
            child: CustomTextFormField(
              context: context,
              label: 'Password',
              hint: 'password',
              controller: _passController,
              isObscureText: _isObscurePassword,
              suffixIcon: IconButton(
                icon: Icon(
                  _isObscurePassword
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                ),
                onPressed: () {
                  setState(() {
                    _isObscurePassword = !_isObscurePassword;
                  });
                },
              ),
            ),
          ),

        ],
      ),
    );
  }

  void registerProcess() async {
    final _email = _mailController.value;
    final _password = _passController.value;
    final _username = _usernameController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null
    ) {


      User user = User(
        userName: _username,
        email: _email,
        password: _password,
      );
      authBlocCubit.register_user(user);
      Fluttertoast.showToast(
          msg: "Register Berhasil",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
      Navigator.pop(context);
    }else if(formKey.currentState?.validate() == false &&
        _email != null &&
        _password != null){
      Fluttertoast.showToast(
          msg: "Masukkan e-mail yang valid",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    }else if(formKey.currentState?.validate() == false ||
        formKey.currentState?.validate() == true && _email == null &&
        _password == null){
      Fluttertoast.showToast(
          msg: "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.white,
          textColor: Colors.black,
          fontSize: 16.0);
    }
  }

}