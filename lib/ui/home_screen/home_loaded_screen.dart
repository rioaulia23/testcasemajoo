import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail_screen/detail_movie_screen.dart';


import 'package:majootestcase/ui/login_screen/login_screen.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Results> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthBlocCubit authBlocCubit = AuthBlocCubit();
    return BlocBuilder(
      cubit: authBlocCubit,
        builder: (context,state){
      return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: GestureDetector(
                  onTap: (){
                    authBlocCubit.logout();
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (_) => LoginScreen(),
                      ),
                    );
                  },
                  child: Icon(Icons.logout)),
            )
          ],
        ),
        body: ListView.builder(
          itemCount: data.length,

          itemBuilder: (context, index) {
            return movieItemWidget(data[index],context);
          },
        ),
      );
    });
  }

  Widget movieItemWidget(Results data,context){
    return GestureDetector(

      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) => DetailScreen(data)
          ),
        );
      },
      child:Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)
            )
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network("https://image.tmdb.org/t/p/w500/"+data.posterPath),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.name ?? data.originalTitle, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }
}
