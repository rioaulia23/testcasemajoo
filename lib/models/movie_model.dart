class MovieModel {
  int page;
  List<Results> results;
  int totalPages;
  int totalResults;

  MovieModel({this.page, this.results, this.totalPages, this.totalResults});

  MovieModel.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    if (json['results'] != null) {
      results = new List<Results>();
      json['results'].forEach((v) {
        results.add(new Results.fromJson(v));
      });
    }
    totalPages = json['total_pages'];
    totalResults = json['total_results'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    data['total_pages'] = this.totalPages;
    data['total_results'] = this.totalResults;
    return data;
  }
}

class Results {
  String originalTitle;
  String posterPath;
  bool video;
  double voteAverage;
  String title;
  int voteCount;
  int id;
  String releaseDate;
  bool adult;
  String overview;
  List<int> genreIds;
  String backdropPath;
  String originalLanguage;
  double popularity;
  String mediaType;
  List<String> originCountry;
  String name;
  String firstAirDate;
  String originalName;

  Results(
      {this.originalTitle,
        this.posterPath,
        this.video,
        this.voteAverage,
        this.title,
        this.voteCount,
        this.id,
        this.releaseDate,
        this.adult,
        this.overview,
        this.genreIds,
        this.backdropPath,
        this.originalLanguage,
        this.popularity,
        this.mediaType,
        this.originCountry,
        this.name,
        this.firstAirDate,
        this.originalName});

  Results.fromJson(Map<String, dynamic> json) {
    originalTitle = json['original_title'];
    posterPath = json['poster_path'];
    video = json['video'];
    voteAverage = json['vote_average'];
    title = json['title'];
    voteCount = json['vote_count'];
    id = json['id'];
    releaseDate = json['release_date'];
    adult = json['adult'];
    overview = json['overview'];
    genreIds = json['genre_ids'].cast<int>();
    backdropPath = json['backdrop_path'];
    originalLanguage = json['original_language'];
    popularity = json['popularity'];
    mediaType = json['media_type'];
    originCountry = json['name'] != null
        ? json['origin_country'].cast<String>()
        : [];
    name = json['name'];
    firstAirDate = json['first_air_date'];
    originalName = json['original_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['original_title'] = this.originalTitle;
    data['poster_path'] = this.posterPath;
    data['video'] = this.video;
    data['vote_average'] = this.voteAverage;
    data['title'] = this.title;
    data['vote_count'] = this.voteCount;
    data['id'] = this.id;
    data['release_date'] = this.releaseDate;
    data['adult'] = this.adult;
    data['overview'] = this.overview;
    data['genre_ids'] = this.genreIds;
    data['backdrop_path'] = this.backdropPath;
    data['original_language'] = this.originalLanguage;
    data['popularity'] = this.popularity;
    data['media_type'] = this.mediaType;
    data['origin_country'] = this.originCountry;
    data['name'] = this.name;
    data['first_air_date'] = this.firstAirDate;
    data['original_name'] = this.originalName;
    return data;
  }
}
